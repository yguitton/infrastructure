# This is a sample file distributed with Galaxy that is used to define a
# list of protein BLAST databases, using three columns tab separated:
#
# <unique_id>{tab}<database_caption>{tab}<base_name_path>
#
# The captions typically contain spaces and might end with the build date.
# It is important that the actual database name does not have a space in
# it, and that there are only two tabs on each line.
#
# You can download the NCBI provided protein databases like NR from here:
# ftp://ftp.ncbi.nlm.nih.gov/blast/db/
#
# For simplicity, many Galaxy servers are configured to offer just a live
# version of each NCBI BLAST database (updated with the NCBI provided
# Perl scripts or similar). In this case, we recommend using the case
# sensistive base-name of the NCBI BLAST databases as the unique id.
# Consistent naming is important for sharing workflows between Galaxy
# servers.
#
# For example, consider the NCBI "non-redundant" protein BLAST database
# where you have downloaded and decompressed the files under /data/blastdb/
# meaning at the command line BLAST+ would be run with something like
# which would look at the files /data/blastdb/nr.p*:
#
# $ blastp -db /data/blastdb/nr -query ...
#
# In this case use nr (lower case to match the NCBI file naming) as the
# unique id in the first column of blastdb_p.loc, giving an entry like
# this:
#
# nr{tab}NCBI non-redundant (nr){tab}/data/blastdb/nr
#
# Alternatively, rather than a "live" mirror of the NCBI databases which
# are updated automatically, for full reproducibility the Galaxy Team
# recommend saving date-stamped copies of the databases. In this case
# your blastdb_p.loc file should include an entry per line for each
# version you have stored. For example:
#
# nr_05Jun2010{tab}NCBI NR (non redundant) 05 Jun 2010{tab}/data/blastdb/05Jun2010/nr
# nr_15Aug2010{tab}NCBI NR (non redundant) 15 Aug 2010{tab}/data/blastdb/15Aug2010/nr
# ...etc...
#
# See also blastdb.loc which is for any nucleotide BLAST database, and
# blastdb_d.loc which is for any protein domains databases (like CDD).
ARATH_2020-02_prot	Arabidopsis thaliana (Araport11 2020-02)_prot	/shared/bank/arabidopsis_thaliana/ARAPORT11/fasta/Athaliana_447_Araport11.protein.fa
ARATH_2020-02_cds	Arabidopsis thaliana (Araport11 2020-02)_cds	/shared/bank/arabidopsis_thaliana/ARAPORT11/fasta/Athaliana_447_Araport11.cds.fa
MUSAC_2022-08_prot	Musa acuminata ssp malaccensis (4.3_CIRAD_SEG 2022-08)_prot	/shared/bank/musa_acuminata/Musa_acuminata_ssp_malaccensis_4.3_CIRAD_SEG/fasta/Musa_acuminata_pahang_v4.pep.fa
MUSAC_2022-08_cds	Musa acuminata ssp malaccensis (4.3_CIRAD_SEG 2022-08)_cds	/shared/bank/musa_acuminata/Musa_acuminata_ssp_malaccensis_4.3_CIRAD_SEG/fasta/Musa_acuminata_pahang_v4.cds.fa
SORBI_2017-06_prot	Sorghum bicolor (BTx623_454_v3.1.1 2017-06)_prot	/shared/bank/sorghum_bicolor/BTx623_454_v3.1.1/fasta/Sbicolor_454_v3.1.1.protein.fa
SORBI_2017-06_cds	Sorghum bicolor (BTx623_454_v3.1.1 2017-06)_cds	/shared/bank/sorghum_bicolor/BTx623_454_v3.1.1/fasta/Sbicolor_454_v3.1.1.cds.fa
SACSP_2019-01_prot	Saccharum spontaneum (AP85-441 2019-01)_prot	/shared/bank/saccharum_spontaneum/AP85-441/fasta/Saccharum_spontaneum_AP85-441.protein.fasta
SACSP_2019-01_cds	Saccharum spontaneum (AP85-441 2019-01)_cds	/shared/bank/saccharum_spontaneum/AP85-441/fasta/Saccharum_spontaneum_AP85-441.cds.fasta
SACSP_2018-07_prot	Saccharum spontaneum (hybrid_cultivar_R570_monoploid_BAC_2018-07)_prot	/shared/bank/saccharum_spontaneum/hybrid_cultivar_R570_monoploid/fasta/Saccharum_hybrid_cultivar_R570_monoploid_BAC.protein.faa
SACSP_2018-07_prot	Saccharum spontaneum (hybrid_cultivar_R570_monoploid_STP_2018-07)_prot	/shared/bank/saccharum_spontaneum/hybrid_cultivar_R570_monoploid/fasta/Saccharum_hybrid_cultivar_R570_monoploid_STP.protein.faa
SACSP_2018-07_cds	Saccharum spontaneum (hybrid_cultivar_R570_monoploid_BAC_2018-07)_cds	/shared/bank/saccharum_spontaneum/hybrid_cultivar_R570_monoploid/fasta/Saccharum_hybrid_cultivar_R570_monoploid_BAC.cds.fna
SACSP_2018-07_cds	Saccharum spontaneum (hybrid_cultivar_R570_monoploid_STP_2018-07)_cds	/shared/bank/saccharum_spontaneum/hybrid_cultivar_R570_monoploid/fasta/Saccharum_hybrid_cultivar_R570_monoploid_STP.cds.fna
ORYRUw1654_2013-09_prot	Oryza rufipogon (w1654 2013-09)_prot	/shared/bank/oryza_rufipogon/w1654/fasta/Oryza_rufipogon_w1654.prot.faa
ORYRUw1654_2013-09_cds	Oryza rufipogon (w1654 2013-09)_cds	/shared/bank/oryza_rufipogon/w1654/fasta/Oryza_rufipogon_w1654.cds.fna
ORYRU_2014-12_prot	Oryza rufipogon (w1943 2014-12)_prot	/shared/bank/oryza_rufipogon/w1943/fasta/Oryza_rufipogon.prot.faa
ORYRU_2014-12_cds	Oryza rufipogon (w1943 2014-12)_cds	/shared/bank/oryza_rufipogon/w1943/fasta/Oryza_rufipogon.cds.fna
ORYPU_2014-02_prot	Oryza punctata (v1.2 2014-02)_prot	/shared/bank/oryza_punctata/v1.2/fasta/Oryza_punctata.prot.faa
ORYPU_2014-02_cds	Oryza punctata (v1.2 2014-02)_cds	/shared/bank/oryza_punctata/v1.2/fasta/Oryza_punctata.cds.fna
ORYNI_2018-07_prot	Oryza nivara (v1.0 2018-07)_prot	/shared/bank/oryza_nivara/v1.0/fasta/Oryza_nivara.prot.faa
ORYNI_2018-07_cds	Oryza nivara (v1.0 2018-07)_cds	/shared/bank/oryza_nivara/v1.0/fasta/Oryza_nivara.cds.fna
ORYME_2018-07_prot	Oryza meridionalis  (v1.3 2018-07)_prot	/shared/bank/oryza_meridionalis/v1.3/fasta/Oryza_meridionalis.prot.faa
ORYME_2018-07_cds	Oryza meridionalis  (v1.3 2018-07)_cds	/shared/bank/oryza_meridionalis/v1.3/fasta/Oryza_meridionalis.cds.fna
ORYLO_2018-07_prot	Oryza longistaminata (v1.0 2018-07)_prot	/shared/bank/oryza_longistaminata/v1.0/fasta/Oryza_longistaminata.prot.faa
ORYLO_2018-07_cds	Oryza longistaminata (v1.0 2018-07)_cds	/shared/bank/oryza_longistaminata/v1.0/fasta/Oryza_longistaminata.cds.fna
ORYGA_2018-07_prot	Oryza glaberrima (v1 2018-07)_prot	/shared/bank/oryza_glaberrima/v1/fasta/Oryza_glaberrima.prot.faa
ORYGA_2018-07_cds	Oryza glaberrima (v1 2018-07)_cds	/shared/bank/oryza_glaberrima/v1/fasta/Oryza_glaberrima.cds.fna
ORYBR_2018-07_prot	Oryza brachyantha (v1.4b 2018-07)_prot	/shared/bank/oryza_brachyantha/v1.4b/fasta/Oryza_brachyantha.prot.faa
ORYBR_2018-07_cds	Oryza brachyantha (v1.4b 2018-07)_cds	/shared/bank/oryza_brachyantha/v1.4b/fasta/Oryza_brachyantha.cds.fna
ORYBA_2018-07_prot	Oryza barthii (v1_AGI_PacBio 2018-07)_prot	/shared/bank/oryza_barthii/v1_AGI_PacBio/fasta/Oryza_barthii.prot.faa
ORYBA_2018-07_cds	Oryza barthii (v1_AGI_PacBio 2018-07)_cds	/shared/bank/oryza_barthii/v1_AGI_PacBio/fasta/Oryza_barthii.cds.fna
ORYGU_2018-07_prot	Oryza glumaepatula (v1.5 2018-07)_prot	/shared/bank/oryza_glumipatula/v1.5/fasta/Oryza_glumipatula.prot.faa
ORYGU_2018-07_cds	Oryza glumaepatula (v1.5 2018-07)_cds	/shared/bank/oryza_glumipatula/v1.5/fasta/Oryza_glumipatula.cds.fna
ORYSJNipponbare_2020-12_prot	Oryza sativa japonica nipponbare (RapDB_v7 2020-12)_prot	/shared/bank/oryza_sativa_japonica/nipponbare/RapDB_v7/fasta/Oryza_sativa_japonica_Nipponbare.prot.faa
ORYSJNipponbare_2020-12_cds	Oryza sativa japonica nipponbare (RapDB_v7 2020-12)_cds	/shared/bank/oryza_sativa_japonica/nipponbare/RapDB_v7/fasta/Oryza_sativa_japonica_Nipponbare.cds.fna
ORYSJKitaake_2020-12_prot	Oryza sativa japonica kitaake (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_japonica/kitaake/v1.0/fasta/Oryza_sativa_japonica_Kitaake.prot.faa
ORYSJKitaake_2020-12_cds	Oryza sativa japonica kitaake (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_japonica/kitaake/v1.0/fasta/Oryza_sativa_japonica_Kitaake.cds.fna
ORYSJsubtropchaomeo_2020-12_prot	Oryza sativa japonica subtrop chao meo (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_japonica/subtrop_chao_meo/v1.0/fasta/Oryza_sativa_japonica_subtrop_chao_meo.prot.faa
ORYSJsubtropchaomeo_2020-12_cds	Oryza sativa japonica subtrop chao meo (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_japonica/subtrop_chao_meo/v1.0/fasta/Oryza_sativa_japonica_subtrop_chao_meo.cds.fna
ORYSJtrop1azucena_2020-12_prot	Oryza sativa japonica trop1 azucena (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_japonica/trop1_azucena/v1.0/fasta/Oryza_sativa_japonica_trop1_azucena.prot.faa
ORYSJtrop1azucena_2020-12_cds	Oryza sativa japonica trop1 azucena (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_japonica/trop1_azucena/v1.0/fasta/Oryza_sativa_japonica_trop1_azucena.cds.fna
ORYSJtrop2ketannangka_2020-12_prot	Oryza sativa japonica trop2 ketan nangka (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_japonica/trop2_ketan_nangka/v1.0/fasta/Oryza_sativa_japonica_trop2_ketan_nangka.prot.faa
ORYSJtrop2ketannangka_2020-12_cds	Oryza sativa japonica trop2 ketan nangka (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_japonica/trop2_ketan_nangka/v1.0/fasta/Oryza_sativa_japonica_trop2_ketan_nangka.cds.fna
ORYSI1B1IR64_2020-12_prot	Oryza sativa indica 1B1IR64 (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_indica/1B1_IR64/v1.0/fasta/Oryza_sativa_indica_1B1_IR64.prot.faa
ORYSI1B1IR64_2020-12_cds	Oryza sativa indica 1B1IR64 (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_indica/1B1_IR64/v1.0/fasta/Oryza_sativa_indica_1B1_IR64.cds.fna
ORYSI1B2PR106_2020-12_prot	Oryza sativa indica 1B2PR106 (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_indica/1B2_PR106/v1.0/fasta/Oryza_sativa_indica_1B2_PR106.prot.faa
ORYSI1B2PR106_2020-12_cds	Oryza sativa indica 1B2PR106 (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_indica/1B2_PR106/v1.0/fasta/Oryza_sativa_indica_1B2_PR106.cds.fna
ORYSI2Agobolsail_2020-12_prot	Oryza sativa indica 2A gobol sail (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_indica/2A_gobol_sail/v1.0/fasta/Oryza_sativa_indica_2A_gobol_sail.prot.faa
ORYSI2Agobolsail_2020-12_cds	Oryza sativa indica 2A gobol sail (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_indica/2A_gobol_sail/v1.0/fasta/Oryza_sativa_indica_2A_gobol_sail.cds.fna
ORYSI2Blarhamugad_2020-12_prot	Oryza sativa indica 2B larha mugad (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_indica/2B_larha_mugad/v1.0/fasta/Oryza_sativa_indica_2B_larha_mugad.prot.faa
ORYSI2Blarhamugad_2020-12_cds	Oryza sativa indica 2B larha mugad (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_indica/2B_larha_mugad/v1.0/fasta/Oryza_sativa_indica_2B_larha_mugad.cds.fna
ORYSI3Alima_2020-12_prot	Oryza sativa indica 3A lima (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_indica/3A_lima/v1.0/fasta/Oryza_sativa_indica_3A_lima.prot.faa
ORYSI3Alima_2020-12_cds	Oryza sativa indica 3A lima (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_indica/3A_lima/v1.0/fasta/Oryza_sativa_indica_3A_lima.cds.fna
ORYSI3B1khaoyaiguang_2020-12_prot	Oryza sativa indica 3B1 khao yai guang (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_indica/3B1_khao_yai_guang/v1.0/fasta/Oryza_sativa_indica_3B1_khao_yai_guang.prot.faa
ORYSI3B1khaoyaiguang_2020-12_cds	Oryza sativa indica 3B1 khao yai guang (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_indica/3B1_khao_yai_guang/v1.0/fasta/Oryza_sativa_indica_3B1_khao_yai_guang.cds.fna
ORYSI3B2liuxu_2020-12_prot	Oryza sativa indica 3B2 liu xu (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_indica/3B2_liu_xu/v1.0/fasta/Oryza_sativa_indica_3B2_liu_xu.prot.faa
ORYSI3B2liuxu_2020-12_cds	Oryza sativa indica 3B2 liu xu (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_indica/3B2_liu_xu/v1.0/fasta/Oryza_sativa_indica_3B2_liu_xu.cds.fna
ORYSIIR8_2020-12_prot	Oryza sativa indica IR8 (v1.0 2016-12)_prot	/shared/bank/oryza_sativa_indica/IR8/v1.0/fasta/Oryza_sativa_indica_IR8.prot.faa
ORYSIIR8_2020-12_cds	Oryza sativa indica IR8 (v1.0 2016-12)_cds	/shared/bank/oryza_sativa_indica/IR8/v1.0/fasta/Oryza_sativa_indica_IR8.cds.fna
ORYSIMH63_2020-12_prot	Oryza sativa indica minghui63 (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_indica/minghui63/v3.0/fasta/Oryza_sativa_indica_MH63.prot.faa
ORYSIMH63_2020-12_cds	Oryza sativa indica minghui63 (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_indica/minghui63/v3.0/fasta/Oryza_sativa_indica_MH63.cds.fna
ORYSIR498_2020-12_prot	Oryza sativa indica shuhui498 (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_indica/shuhui498/v1.0/fasta/Oryza_sativa_indica_R498.prot.faa
ORYSIR498_2020-12_cds	Oryza sativa indica shuhui498 (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_indica/shuhui498/v1.0/fasta/Oryza_sativa_indica_R498.cds.fna
ORYSIZS97_2020-12_prot	Oryza sativa indica zhenshan97 (v3.0 2020-12)_prot	/shared/bank/oryza_sativa_indica/zhenshan97/v3.0/fasta/Oryza_sativa_indica_ZS97.prot.faa
ORYSIZS97_2020-12_cds	Oryza sativa indica zhenshan97 (v3.0 2020-12)_cds	/shared/bank/oryza_sativa_indica/zhenshan97/v3.0/fasta/Oryza_sativa_indica_ZS97.cds.fna
ORYSAN22_2020-12_prot	Oryza sativa aus N22 (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_aus/N22/v1.0/fasta/Oryza_sativa_aus_N22.prot.faa
ORYSAN22_2020-12_cds	Oryza sativa aus N22 (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_aus/N22/v1.0/fasta/Oryza_sativa_aus_N22.cds.fna
ORYSANatelBoro_2020-12_prot	Oryza sativa aus natel boro (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_aus/natel_boro/v1.0/fasta/Oryza_sativa_aus_natel_boro.prot.faa
ORYSANatelBoro_2020-12_cds	Oryza sativa aus natel boro (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_aus/natel_boro/v1.0/fasta/Oryza_sativa_aus_natel_boro.cds.fna
ORYSBARC10497_2020-12_prot	Oryza sativa basmati ARC10497 (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_basmati/ARC10497/v1.0/fasta/Oryza_sativa_basmati_ARC10497.prot.faa
ORYSBARC10497_2020-12_cds	Oryza sativa basmati ARC10497 (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_basmati/ARC10497/v1.0/fasta/Oryza_sativa_basmati_ARC10497.cds.fna
ORYSB334_2020-12_prot	Oryza sativa basmati basmati334 (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_basmati/basmati334/v1.0/fasta/Oryza_sativa_basmati_Basmati334.prot.faa
ORYSB334_2020-12_cds	Oryza sativa basmati basmati334 (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_basmati/basmati334/v1.0/fasta/Oryza_sativa_basmati_Basmati334.cds.fna
ORYSBDomsufid_2020-12_prot	Oryza sativa basmati domsufid (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_basmati/domsufid/v1.0/fasta/Oryza_sativa_basmati_Domsufid.prot.faa
ORYSBDomsufid_2020-12_cds	Oryza sativa basmati domsufid (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_basmati/domsufid/v1.0/fasta/Oryza_sativa_basmati_Domsufid.cds.fna
ORYSN4_2020-12_prot	Oryza sativa nerica nerica4 (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_nerica/nerica4/v1.0/fasta/Oryza_sativa_Nerica4.prot.faa
ORYSN4_2020-12_cds	Oryza sativa nerica nerica4 (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_nerica/nerica4/v1.0/fasta/Oryza_sativa_Nerica4.cds.fna
ORYSNL19_2020-12_prot	Oryza sativa nerica nericaL19 (v1.0 2020-12)_prot	/shared/bank/oryza_sativa_nerica/nericaL19/v1.0/fasta/Oryza_sativa_NericaL19.prot.faa
ORYSNL19_2020-12_cds	Oryza sativa nerica nericaL19 (v1.0 2020-12)_cds	/shared/bank/oryza_sativa_nerica/nericaL19/v1.0/fasta/Oryza_sativa_NericaL19.cds.fna
COCNU_2017-10_prot	Cocos nucifera (COCNU_HT001R02O02_2017-10)_prot	/shared/bank/cocos_nucifera/COCNU_HT001R02O02/fasta/COCNU_HT001R02O02-Liftoff-polypeptide-locus_tag-genfam_representative-longest.faa
ELAGV_2013-08_prot	Elaeis guineensis (ELAGV_AVROS001RR102_2013-08)_prot	/shared/bank/elaeis_guineensis/ELAGV_AVROS001RR102/fasta/GCF_000442705.1_EG5_longest_protein_id.faa
PHODC_2013-06_prot	Phoenix dactylifera (PHODC_Khalas002RR102_2013-06)_prot	/shared/bank/phoenix_dactylifera/PHODC_Khalas002RR102/fasta/GCF_000413155.1_DPV01_longest_protein_id.faa
uniprot_swissprot_2018_10_10	UniProt/SwissProt (2018-10-10)	/shared/bank/uniprot_swissprot/uniprot_swissprot_2018-10-10/blast/uniprot_swissprot
uniprot_2018_08	UniProt (2018-08)	/shared/bank/uniprot/uniprot_2018_08/blast/uniprot
uniprot_2021_02	UniProt (2021_02)	/shared/bank/uniprot/uniprot_2021_02/blast/uniprot
uniprot_swissprot_2020_03	UniProt SwissProt (2020-03)	/shared/bank/uniprot_swissprot/uniprot_swissprot_2020_03/blast/uniprot_swissprot
uniprot_2023_02	UniProt (2023_02)	/shared/bank/uniprot/uniprot_2023_02/blast/uniprot_2023_02
